module.exports.iniciaChat = function(application, req, res){
    
    const dadosForm = req.body
    req.assert('apelido', 'Apelido obrigatório').notEmpty()
    req.assert('apelido', 'Apelido entre 3 e 15 caracteres').len(3, 15)

    const errors = req.validationErrors()

    if(errors) {
        res.render('index', { validation: errors })
        return
    }

    application.get('io').emit(
        'msgParaCliente',
        { apelido: dadosForm.apelido, mensagem: ' entrou' }
    )
    
    res.render('chat', { dadosForm: dadosForm })
}